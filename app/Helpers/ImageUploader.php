<?php

namespace App\Helpers;

use Auth;
use Image;
use Storage;

/**
* Image Uploader Class
*/
class ImageUploader
{
	/**
	 * default blob
	 * @var blob
	 */
	protected $image;

	/**
	 * Default diskname
	 * @var string
	 */
	protected $defaultDisk = 'pub';

	/**
	 * default path
	 * @var string
	 */
	protected $defaultPath = 'pictures';	

	/**
	 * Array attributes image
	 * @var array
	 */
	protected $attributes = [];
	
	public function __construct ($requestImage)
	{
		$this->image 	= $requestImage;
		$this->filename = strtolower(str_random(30)) . ".jpg";

		return $this;
	}

	public function save ($width = 0, $height = 0)
	{
		if ($width && $height)
		{
			$fileName = $this->filename;
			$filePath = [$this->defaultPath, $width, $height, $fileName];

			$image 	  = Image::make($this->image)->resize($width, $height)->encode('jpg');

			$path 	  = implode('/', $filePath);

			Storage::disk($this->defaultDisk)->put($path, $image);
		}

		return $this;
	}
}