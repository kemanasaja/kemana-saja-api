<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Helpers\Response;
use App\Helpers\ImageUploader;


class AccountController extends Controller
{

    public function create (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'address'   => 'required',
            'email'     => 'required',
            'mobile'    => 'required',
            'password'  => 'required'
        ]);

        if ($validator->errors()->count())
        {
            return Response::badRequest($validator->errors());
        }

        $users = new User();

        $user = $users->createUser($request);

        if ($user)
        {
            return Response::created($user);
        }

        return Response::internalError('Unable to create user');
    }

    public function show ()
    {
        $user = Auth::user();

        if (!$user)
        {
            return Response::notFound('User not found');
        }

        return Response::json($user);
    }

    public function update (Request $request)
    {

        $user = Auth::user();

        $data = [
            'name'      => $request->name,
            'address'   => $request->address,
            'email'     => $request->email,
            'mobile'    => $request->mobile
        ];

        $result = $user->update($data);

        if ($result)
        {
            return Response::json($user);
        }

        return Response::internalError('Unable to update user profile');
    }

    public function picture ()
    {

        $img = new ImageUploader($this->request->image);

        if ($bg = $img->save(300, 300) &&
            $sm = $img->save(100, 100) &&
            $xs = $img->save(50, 50))
        {
            $picture = ['picture' => $img->filename];

            Auth::user()->update($picture);

            return Response::json($picture);
        }

        return Response::internalError('Unable to upload image');

    }
}
