<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;

use Auth;
use App\Models\User;
use App\Models\Location;
use App\Helpers\Response;
use Carbon\Carbon;
use DateTime;

class LocationController extends Controller
{
	protected $user;
	protected $request;
    protected $locations = [];

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct (Request $request)
    {
        $this->user     = Auth::user();
        $this->request  = $request;
    }

    public function index (Request $request)
    {   
        $user = Auth::user();

        $request->query('address') || $request->query('begin') || $request->query('end')
            ? $locations = $this->gatherLocations($request)
            : $locations = $user->Location;

        if ($locations)
        {
    	   return Response::json($locations);
        }

        return Response::notFound('Location not found');
    }

    private function gatherLocations (Request $request) {

        $this->parseAddress($request->query('address'));
        $this->parseRange($request->query('begin'), $request->query('end'));

        $location = $this->locations;

        $newLocation = [];

        foreach ($location as $loc) {
            $date = new DateTime($loc['check_in_time']);
            $loc['check_in_time'] = $date->format('Y-m-d,H:m');
            array_push($newLocation, $loc);
        }

        return $newLocation;

    }

    private function parseAddress (String $address = null) {

        if (is_null($address) || $address == '') {
            return;
        }

        $loc = Location::where("address", "LIKE", "%{$address}%")->get();

        $this->locations = $loc->toArray();

    }

    private function parseRange (String $begin = null, String $end = null) {

        if ($begin == null || $begin == '') {
            return;
        }

        if ($end == null || $end == '') {
            return;
        }

        $loc = Location::whereBetween('check_in_time', [$begin . ' 00:00:00', $end . ' 23:59:59'])->orderBy('check_in_time', 'desc')->get();

        if (is_null($loc)) {
            return;
        }

        $this->locations = $this->locations + $loc->toArray();

    }

    public function show ($id)
    {
    	$location = Location::find($id);

    	if (!$location)
    	{
    		return Response::notFound('Location not found');
    	}

    	return Response::json($location);
    }

    public function create (Request $request)
    {
        $user = Auth::user();
    	$locations = collect($request)->toArray();

        $data = [];

        foreach ($locations as $location) {
            $date = new Datetime($location['check_in_time']);
            $d = [
                'user_id'       => $user->id,
                'lng'           => $location['lng'],
                'lat'           => $location['lat'],
                'address'       => $location['address'],
                'check_in_time' => $date
            ];

            $res = Location::create($d);
            $res['check_in_time'] = $date = $date->format('Y-m-d,H:m');
            array_push($data, $res);
        }

        $response = Response::json($data);

        return $response;
    }

    public function create2 (Request $request)
    {
        $user = Auth::user();

        $location = collect($request)->toArray();

        $date = new Datetime($request['check_in_time']);

        $location['user_id']         = Auth::user()->id;
        $location['check_in_time']   = $date;

        $loc = Location::create($location);
        $location['check_in_time']   = $date->format('Y-m-d,H:m');

        if ($loc) {
            return Response::json($location);
        }

        return Response::internalError('Something wrong');
   
    }

    public function create3 (Request $request)
    {
        $user = Auth::user();
        $location = $request->data;

        // $location = substr($location, 1, -1);

        // $arrObj = explode('},',$location);
        // $data = [];

        // for($i=0; $i<count($arrObj); $i++) {

        //     if ($i < count($arrObj) -1) {
        //         array_push($data, $arrObj[$i]);
        //     }
            
        // }

        // return $json_parse(data);
    }

    public function delete ($id)
    {
    	try
    	{
    		$location = Location::find($id);

    		if ($location)
    		{
    			$location->delete();
    			return Response::deleted();
    		}

    		return Response::notFound('Location not found');
    	}
    	catch (Exception $e)
    	{
    		return Response::internalError('Unable to delete location');
    	}
    }
}