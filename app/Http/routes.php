<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['prefix' => '/', 'namespace' => 'App\Http\Controllers'], function () use ($app) {

	$app->get('/', function () use ($app) {
	    return view('welcome');
	});

	$app->get('docs', function () use ($app) {
		return view('docs');
	});

	$app->post('register', ['uses' => 'AccountController@create', ]);

});

/*
|--------------------------------------------------------------------------
| After Login
|--------------------------------------------------------------------------
|
| Here is the available routes when user login successfully
|
*/

$app->group(['prefix' => 'api/v1', 'namespace' => 'App\Http\Controllers', 'middleware' => ['auth']], function () use ($app) {

	$app->get('profile', 			['uses' => 'AccountController@show']);
	$app->put('profile', 			['uses' => 'AccountController@update']);
	$app->post('profile-picture', 	['uses' => 'AccountController@picture']);

	$app->get('location', 			['uses' => 'LocationController@index']);
	$app->get('location/{id}', 		['uses' => 'LocationController@show']);
	$app->post('location', 			['uses' => 'LocationController@create']);
	$app->delete('location/{id}', 	['uses' => 'LocationController@delete']);

	$app->post('location2',	['uses' => 'LocationController@create2']);
	$app->post('location3',	['uses' => 'LocationController@create3']);

});
