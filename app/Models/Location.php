<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	protected $fillable = [
		'user_id', 'lng', 'lat', 'address', 'check_in_time'
	];

	protected $hidden = [
		'created_at', 'updated_at'
	];

	public function user ()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}