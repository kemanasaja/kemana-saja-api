<?php

namespace App\Models;

use App\Models\Location;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'email', 'mobile', 'picture', 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'created_at', 'updated_at'
    ];

    public function getUser ($id)
    {
        return $this->find($id);
    }

    public function createUser ($input)
    {
        $user = new $this;

        $user->name = $input->name;
        $user->address = $input->address;
        $user->email = $input->email;
        $user->mobile = $input->mobile;
        $user->password = (new BcryptHasher)->make($input->password);

        $user->save();

        return $user;
        //return $this->create($input->all());
    }

    public function Location ()
    {
        return $this->hasMany(Location::class);
    }
}
