# Login
---

Method `POST`

URL : ```/oauth/token```

Parameter

* `string : grant_type` : password
* `string : client_id` : 2
* `string : client_secret`: NVryUmasU9HFZZguzcFSmmFAzdotFRD6uLL4k4QS
* `string : username`
* `string : password`


#### Response

**Success** (200)

* `token_type`
* `expires_in`
* `access_token`
* `refresh_token`

**Error** (400) (500)
