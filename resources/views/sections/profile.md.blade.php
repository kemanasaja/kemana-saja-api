# Profile (Show)
---

Method `GET`

URL : ```/api/v1/profile```

Header

* `Authorization: Bearer { access_token }`

#### Response

**Success** (200)

* `Object`

**Error** (400) (500)


# Profile (Update)
---

Mehtod `PUT`

url : ```/api/v1/profile```

Header

* `Authorization: Bearer { access_token }`
* `Content-Type: application/x-www-form-urlencoded`

Parameter

* `string : name`
* `string : address`
* `string : email`
* `string : mobile`
* `string : picture`
* `string : password`