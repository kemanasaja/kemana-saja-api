# Location (Get)
---

Method `GET`

URL : ```/api/v1/location```

URL : ```/api/v1/location?address={address}```

URL : ```/api/v1/location?begin={begin_time}&end={end_time}```

URL : ```/api/v1/location?address={address}&begin={begin_time}&end={end_time}```

Header

* `Authorization: Bearer { access_token }`

# Location (Create)
---

Method `POST`

URL : ```/api/v1/location```

Header

* `Authorization: Bearer { access_token }`
* `Accept: application/json`
* `Content-Type: application/json`

Parameter

* `string : longitude
* `string : latitude


#### Response

**Success** (200)

* `id`
* `user_id`
* `lalitude`
* `longitude`

**Error** (400) (500)

# Location (Delete)
---

Method `DELETE`

URL : ```/api/v1/location/{location_id}```

Header

* `Authorization: Bearer { access_token }`

#### Response

**Success** (200)

**Error** (400) (500)
