<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .half-height {
                height: 50vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .menu-list {
                margin: auto;
                list-style: none;
                padding: 0;
                margin: 0;
                margin-bottom: 5rem;
            }

            .menu-list li {
                text-align: center;
                padding: 0.5rem;
            }

            a {
                text-decoration: none;
            }

            a:hover {
                text-decoration: underline;
            }

            section {
                width: 50%;
                margin: auto;
                margin-bottom: 5rem;
            }
        </style>
    </head>
    <body>
        
        <div class="flex-center position-ref half-height">

            <div class="content">
                <div class="title m-b-md">
                    Kemana Saja API
                </div>

                <div class="links">
                    <a>API Documentation</a>
                </div>
            </div>
        </div>

        <ol class="menu-list">
            <li><a href="#register">Register</a></li>
            <li><a href="#login">Login</a></li>
            <li><a href="#location">Location</a></li>
            <li><a href="#profile">Profile</a></li>
        </ol>

        <section id="register">
            @include('sections.register')
        </section>

        <section id="login">
            @include('sections.login')
        </section>

        <section id="location">
            @include('sections.location')
        </section>

        <section id="profile">
            @include('sections.profile')
        </section>

    </body>
</html>
